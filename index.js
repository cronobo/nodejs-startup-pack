var date = new Date().toISOString();

var msg = `
This is your main script, the one that Cronobo will run by default.
Feel free to modify it at your will.

You can do everything you are used to when using NodeJS with Cronobo.
You can install packages using 'npm install <package> --save'.
You can require() modules, organize your script into multiple files.

You can also use modern ES6 Javascript features.
For instance, you can use string litterals to format strings. Example:

This script ran at ${date}`;

console.log(msg);
