# Nodejs startup pack for Cronobo

This repository contains the few required files to make a script run on Cronobo. (If you are familiar with Docker, it's basically the files required to build a Docker image).

Write your script inside `index.js` then package it by running:

```
npm install
node packer.js
```

After that, you can upload the archive to cronobo.

## What is Cronobo ?

[Cronobo](https://cronobo.com) is a scheduling and data acquisition platform.
Upload a script and it will run at any date or with the recurrence of you choice.

Compared to a manual, VPS-backed approach, cronobo lets you:
- store output data extremely easily
- be notified in case of runtime errors
- plot output data from your script using simple dashboards or Grafana
- run multiple instances of your script in parallel
- and much more.
