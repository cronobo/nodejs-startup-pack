var fs = require('fs');
var path = require('path');
var archiver = require('archiver');
var glob = require('glob');
var archiveName = 'nodejs-cronobo-script.zip';
var output = fs.createWriteStream(__dirname + `/${archiveName}`);

output.on('close', function() {
  console.log('Pack completed. Archive size (bytes): ' + archive.pointer());
});

var archive = archiver('zip', {});
archive.pipe(output);

archive.on('warning', function(err) {
  if (err.code === 'ENOENT') {
      console.warning(err);
  } else {
      throw err;
  }
});

archive.on('error', function(err) {
  throw err;
});

var files = glob.sync('?(*.json|*.js|.dockerignore|Dockerfile|README.md)', {
  dot: true
});

console.log('Archiving files:');
console.log(files);
console.log(`into ${archiveName}`);

for(var i = 0 ; i < files.length ; i++) {
  archive.append(fs.createReadStream(files[i]), {name: path.basename(files[i])});
}
archive.finalize();
